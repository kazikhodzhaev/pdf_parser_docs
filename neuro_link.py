import string
import numpy as np

from keras.models import Sequential
from keras.layers import Dense, LSTM, Embedding, RepeatVector, Dropout
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model
from keras import optimizers
from sklearn.model_selection import train_test_split
from read_pdf_conf_prod import *
from neuro_link_teach_data import DATA, DATA_PAGE_INDEX

import logs


logger = logs.get_logger(__name__, os.getpid())
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


# Encode and pad sequences
def encode_sequences(tokenizer, length, lines):
    # integer encode sequences
    seq = tokenizer.texts_to_sequences(lines)
    # pad sequences with 0 values
    seq = pad_sequences(seq, maxlen=length, padding='post')
    return seq


# Build NMT model
def make_model(in_vocab, out_vocab, in_timesteps, out_timesteps, n):
    model = Sequential()
    model.add(Embedding(in_vocab, n, input_length=in_timesteps, mask_zero=True))
    model.add(LSTM(n))
    model.add(Dropout(0.3))
    model.add(RepeatVector(out_timesteps))
    model.add(LSTM(n, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(Dense(out_vocab, activation='softmax'))
    model.compile(optimizer=optimizers.RMSprop(lr=0.001), loss='sparse_categorical_crossentropy')
    return model


def tokenize_txt(data, length):
    recognize_doc_type_fail_doc_type = np.array(data)
    recognize_doc_type_fail_doc_type = recognize_doc_type_fail_doc_type[:30000, :]

    # Remove punctuation
    recognize_doc_type_fail_doc_type[:, 0] = [s.translate(str.maketrans('', '', string.punctuation)) for s in recognize_doc_type_fail_doc_type[:, 0]]
    recognize_doc_type_fail_doc_type[:, 1] = [s.translate(str.maketrans('', '', string.punctuation)) for s in recognize_doc_type_fail_doc_type[:, 1]]

    # Convert text to lowercase
    for i in range(len(recognize_doc_type_fail_doc_type)):
        recognize_doc_type_fail_doc_type[i, 0] = recognize_doc_type_fail_doc_type[i, 0].lower()
        recognize_doc_type_fail_doc_type[i, 1] = recognize_doc_type_fail_doc_type[i, 1].lower()

    # Prepare fail_doc_typelish tokenizer
    fail_doc_type_tokenizer = Tokenizer()
    fail_doc_type_tokenizer.fit_on_texts(recognize_doc_type_fail_doc_type[:, 0])
    fail_doc_type_vocab_size = len(fail_doc_type_tokenizer.word_index) + 1
    fail_doc_type_length = length

    # Prepare recognize_doc_typetch tokenizer
    recognize_doc_type_tokenizer = Tokenizer()
    recognize_doc_type_tokenizer.fit_on_texts(recognize_doc_type_fail_doc_type[:, 1])
    recognize_doc_type_vocab_size = len(recognize_doc_type_tokenizer.word_index) + 1
    recognize_doc_type_length = length

    return fail_doc_type_tokenizer, recognize_doc_type_tokenizer, fail_doc_type_length, \
           recognize_doc_type_vocab_size, fail_doc_type_vocab_size, recognize_doc_type_length, \
           recognize_doc_type_fail_doc_type


def run(train_type: str):

    if train_type == 'doc_type':
        data = DATA
        file_name = '../doc-type-model.h5'
        length = 5

    if train_type == 'page_index':
        data = DATA_PAGE_INDEX
        file_name = 'models/page-index-model.h5'
        length = 3

    fail_doc_type_tokenizer, recognize_doc_type_tokenizer, fail_doc_type_length, recognize_doc_type_vocab_size, fail_doc_type_vocab_size, recognize_doc_type_length, recognize_doc_type_fail_doc_type = tokenize_txt(data=data, length=length)

    train, test = train_test_split(recognize_doc_type_fail_doc_type, test_size=0.5, random_state=12)
    trainX = encode_sequences(fail_doc_type_tokenizer, fail_doc_type_length, train[:, 0])
    trainY = encode_sequences(recognize_doc_type_tokenizer, recognize_doc_type_length, train[:, 1])
    model = make_model(fail_doc_type_vocab_size, recognize_doc_type_vocab_size, fail_doc_type_length,
                       recognize_doc_type_length, 512)

    # Train model
    num_epochs = 500
    model.fit(trainX, trainY.reshape(trainY.shape[0], trainY.shape[1], 1), epochs=num_epochs, batch_size=512,
              validation_split=0.2, callbacks=None, verbose=1)
    model.save(file_name)


def get_word(n, tokenizer):
    if n == 0:
        return ""
    for word, index in tokenizer.word_index.items():
        if index == n:
            return word
    return ""


def run_recognize(recognize: str, recognize_type: str):
    # Load model
    if recognize_type == 'doc_type':
        model = load_model('models/doc-type-model.h5')
        data = DATA
        length = 5
        recognize = recognize.replace('-', ' ').lower()

    if recognize_type == 'page_index':
        model = load_model('models/page-index-model.h5')
        data = DATA_PAGE_INDEX
        length = 3

    fail_doc_type_tokenizer, recognize_doc_type_tokenizer, fail_doc_type_length, recognize_doc_type_vocab_size, fail_doc_type_vocab_size, recognize_doc_type_length, recognize_doc_type_fail_doc_type = tokenize_txt(data=data, length=length)
    phrs_enc = encode_sequences(fail_doc_type_tokenizer, fail_doc_type_length, [recognize])
    preds = np.argmax(model.predict(phrs_enc), axis=-1)
    result = ''
    recognize_words_id = list(set(preds[0]))

    for i in range(0, len(recognize_words_id)):
        result += f'{get_word(preds[0][i], recognize_doc_type_tokenizer)} '

    if recognize_type == 'page_index':
        return result

    logger.info(result)
    result = result if result.strip() in DOC_TYPES else 'документы'

    return result.strip() if result else None


if __name__ == '__main__':
    run(train_type='doc_type')
    run(train_type='page_index')
