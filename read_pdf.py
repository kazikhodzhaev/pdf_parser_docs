#!/usr/bin/env python3.6
"""
PDF Parser
I.Kazikhodzhaev
"""
import time
import re
import shutil
import uuid
import logs

from datetime import datetime
from collections import Counter
from PyPDF2 import PdfFileWriter, PdfFileReader, utils as pypdf_utils
from tensorflow.python.framework import errors_impl
from neuro_link import run_recognize
from read_pdf_conf_prod import *
from utils import add_log, get_text_from_image, LOG_TYPES, make_dirs, save_in_to_db_files_path, send_req_in_to_one_c
from decorators import timing

logger = logs.get_logger(__name__, os.getpid())


class ReadAndParseScanDocs:
    """
    Остновной класс в котором происходить парсинг,
    сборка отдельных файлов по типу и номеру документа
    """

    def __init__(self, file, path_save_img):
        """
        :param file:
        :param path_save_img:
        """
        self.file = file
        self.guid = str(uuid.uuid4())
        self.dirs_to_save = None
        self.ordered_pages = dict()
        self.pages_counts = dict()
        self.document_number = None
        self.source_file_copied = False
        self.doc_pages = []
        self.path_save_img = path_save_img
        self.folder = datetime.now().date().__str__().replace('-', '')

    @staticmethod
    def _search_data_in_text(text, regex):
        """
        Поиск текста по регулярке
        :param text:
        :param regex:
        :return:
        """
        if re.search(regex, text):
            return re.search(regex, text).group()
        else:
            return None

    @staticmethod
    def _get_max_repeated_num(nums):
        """
        Получение номера документа который встречаеться больше всех остальных
        :param nums:
        :return:
        """
        duplicate_counts = dict(Counter(nums))
        for k, v in duplicate_counts.items():
            if v == max(duplicate_counts.values()):
                return str(k)[1:]

    def _get_doc_num(self, text, regex_list):
        """
        Получение номера документа
        :param text:
        :param regex_list:
        :return:
        """
        num = None
        for regex in regex_list:
            num = self._search_data_in_text(text, regex)
            if num:
                for element in DOC_NUM_REPLACE:
                    if element in num:
                        num = num.replace(element, 'W')
                break
        return num.replace(' ', '') if num else num

    def _copy_file(self, path_to_copy):
        """
        Копирование файла из начальной папки на указанную папку после переноса
        :param path_to_copy:
        :return:
        """
        file_path = os.path.join(path_to_copy, str(self.file.split('/')[-1]))
        shutil.copy(self.file, path_to_copy)
        os.chmod(file_path, 0o777)
        self.source_file_copied = os.path.exists(file_path)

    def _remove_old_file(self):
        """
        Удаление исходного файл из начальной папку после переноса
        :return:
        """
        os.remove(self.file)
        logger.info('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        logger.info(f'REMOVE:{self.file}')
        logger.info('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')

    @timing
    def _get_doc_type(self, text):
        """
        Получение типа документа из титула
        :param text:
        :return:
        """
        document_type = str(text.split('\n')[0].split('№')[0]).replace('-', ' ').replace('—', ' ').strip().lower()
        document_page_index = self._search_data_in_text(document_type, '[0-9А-Яа-я]{1} [0-9А-Яа-я]{2} [0-9А-Яа-я]{1}')
        if document_page_index:
            DOC_TYPE_FIND_SPLIT_STRING_LIST.append(document_page_index)

        if not document_type:
            return 'документы'

        for doc_type in DOC_TYPES:
            if doc_type in document_type:
                logger.info(f'{doc_type} ==> {document_type}')
                document_type = re.search(doc_type.lower(), document_type.lower()).group().lower()
                logger.info(document_type)
                break

        if document_type in DOC_TYPES:
            return document_type

        logger.info(document_type)
        for split_str in DOC_TYPE_FIND_SPLIT_STRING_LIST:
            if split_str in document_type:
                document_type = str(document_type.split(split_str)[0])
                break

        document_type = document_type.lower().strip()
        document_type = run_recognize(recognize=document_type.strip(), recognize_type='doc_type')
        logger.info(document_type)

        return document_type

    @timing
    def _get_document_number(self):
        """
        Выявление номера документа. Проходим по всем страницам что бы получить
        коррекстный номер документа
        :return:
        """
        document_nums = []

        self.doc_pages = [f"{self.path_save_img}/{filename}" for filename in os.listdir(self.path_save_img)]
        for file in self.doc_pages:
            text = get_text_from_image(file=file, number_recognize=True).replace('О', '0').replace('З', '3').replace('з', '3').replace('Б', '5')
            document_num = self._get_doc_num(text, DOCUMENT_NUMER_REGEX)
            if document_num:
                document_nums.append(document_num)

        if not document_nums:
            self._copy_file(path_to_copy=PATH_TO_SAVE_UNSORTED)
            error = "Не известный номер документа"
            add_log(file_path=self.file, error=error, log_type=LOG_TYPES.PARSE_PDF, method=self._collect_pdf.__name__)
            logger.error(f'{error} | {self.file}')
            self._remove_old_file()
            return None

        logger.info(document_nums)
        self.document_number = self._get_max_repeated_num(document_nums)
        self.pages_counts[self.document_number] = dict()
        self.ordered_pages[self.document_number] = dict()

        return self.document_number

    @timing
    def _get_page_index(self, text, document_type, index):
        """
        Получение количество страниц для то что бы понимать все ли страницы
        были обнаружены при парсинге
        :param text:
        :param document_num:
        :param document_type:
        :param index:
        :return:
        """
        page_indexes = None
        page_index = None

        for regex in PAGE_INDEX_REGECS:
            page_indexes = re.search(regex, text.lower()).group() if re.search(regex, text.lower()) else None
            if page_indexes:
                break

        if not page_indexes:
            error = f"Номера страниц не распознаны. Информация: {self.document_number}, {document_type}! Номер стр: {index + 1}"
            logger.error(error)
            add_log(file_path=self.file, document_num=self.document_number, document_type=document_type,
                    error=error, log_type=LOG_TYPES.PARSE_PDF, method=self._get_page_index.__name__)

            return False

        recognize_page_num = self._search_data_in_text(page_indexes.lower(), 'из [0-9]')
        page_count = str(page_indexes).split('из')[1] if recognize_page_num else None

        if page_count:
            try:
                self.pages_counts[self.document_number][document_type] = int(page_count)
            except ValueError:
                return False
            return True

        # Подключаем распознование нейронкой если номера стр не были найдены по маске regex
        try:
            page_indexes = run_recognize(recognize=page_indexes, recognize_type='page_index')
            recognize_page_num = self._search_data_in_text(page_indexes.lower(), 'из [0-9]')
            page_count = str(page_indexes).split(' ')[1] if recognize_page_num else None
            if page_count:
                self.pages_counts[self.document_number][document_type] = int(page_count)
        except (errors_impl.InvalidArgumentError, ValueError):
            error = f"Номер страницы не корректно расспознано! {page_index}. Информация: {self.document_number}, " \
                    f"{document_type}! Номер стр: {index + 1}"
            logger.error(error)
            add_log(file_path=self.file, document_num=self.document_number, document_type=document_type,
                    error=error, log_type=LOG_TYPES.PARSE_PDF, method=self._get_page_index.__name__)
            return False
        return True

    @timing
    def _get_data_to_collect_doc(self):
        """
        Читаем титул и выявляем тип документа.
        Получаем количество страниц.
        Расспределяем страницы по типам.
        :return:
        """
        for file in self.doc_pages:
            text = get_text_from_image(file=file)
            filename = file.split('/')[-1]
            index = int(filename.split('.')[0].split('_')[1])
            document_type = self._get_doc_type(text=text)

            if document_type not in self.pages_counts[self.document_number].keys():
                self._get_page_index(text, document_type, index)

            if document_type in self.ordered_pages[self.document_number].keys():
                self.ordered_pages[self.document_number][document_type].append(index)
            else:
                self.ordered_pages[self.document_number][document_type] = [index]

        logger.info(self.ordered_pages)
        logger.info(self.pages_counts)
        return True

    @timing
    def _collect_doc_pages(self, num, name):
        """
        Проверка документа и сдорка
        :param num:
        :param name:
        :return:
        """
        sourcePdf = PdfFileReader(open(self.file, "rb"))
        doc_data_1c = {"type_doc": name, "id_doc": num, "path": "", "size": "", "err_state": ""}
        file_name = f"{self.dirs_to_save['Out']}/{num}_{name}.pdf".replace(' ', '_')
        doc_data_1c['path'] = f"<path>"
        output = PdfFileWriter()
        outputStream = open(file_name, "wb")
        os.umask(0)

        for index in self.ordered_pages[num][name]:
            output.addPage(sourcePdf.getPage(index))

        if name in self.pages_counts[num] and len(self.ordered_pages[num][name]) == self.pages_counts[num][name]:
            msg = f"Документ {num}, {name} был собран успешно. Путь до файла: {file_name}"
        else:
            msg = f"Номер документа: {num}. Тип документа: {name}. " \
                  f"Количество найденных страниц: {len(self.ordered_pages[num][name])}. " \
                  f"Внимание! Число страниц в файле может не соответствовать оригиналу."
            doc_data_1c["err_state"] = msg

        try:
            output.write(outputStream)
        except pypdf_utils.PdfReadError as e:
            msg = f'ERROR COLLECT DOCUMENT: {e}'
            doc_data_1c["err_state"] = f'ERROR COLLECT DOCUMENT: {e}'
            logger.error(f'{msg} | DOC NUM: {num}')

        outputStream.close()
        os.chmod(file_name, 0o777)
        file_size = os.stat(file_name).st_size
        doc_data_1c['size'] = f"{file_size}"
        save_in_to_db_files_path(file_name, self.document_number)
        add_log(file_path=self.file, document_num=num, document_type=name, msg=msg, log_type=LOG_TYPES.PARSE_PDF,
                method=self._collect_doc_pages.__name__)

        return doc_data_1c

    @timing
    def _collect_pdf(self):
        """
        Проверка распарсенного документа и запуск сборки документов
        :return:
        """
        original_doc_path = os.path.join(self.dirs_to_save['In'], str(self.file.split('/')[-1]))
        data_to_send_1c = {
            "source": f"<path>",
            "docs": [
                {
                    "type_doc": f"Оригинальный скан",
                    "id_doc": f"{self.document_number}",
                    "path": f"<path>",
                    "size": f"{os.stat(original_doc_path).st_size}",
                    "err_state": ""
                }
            ]
        }
        # Сохроняем путь до файла для того что бы потом уменшить размер файла
        save_in_to_db_files_path(original_doc_path, self.document_number)
        logger.info(self.ordered_pages)
        for num, docs in self.ordered_pages.items():
            for name in docs:
                doc_data_1c = self._collect_doc_pages(num, name)
                data_to_send_1c["docs"].append(doc_data_1c)
                logger.info(data_to_send_1c)

        logger.info(data_to_send_1c)
        # Отправка отчета в 1С
        send_req_in_to_one_c(data=data_to_send_1c, doc_num=self.document_number)
        return data_to_send_1c

    @timing
    def run(self):
        """
        Запускаем процесс парсинга по этапно.
        :return:
        """
        self._get_document_number()
        if not self.document_number:
            return False

        self.dirs_to_save = make_dirs(guid=self.guid)
        self._copy_file(path_to_copy=self.dirs_to_save["In"])
        self._get_data_to_collect_doc()
        self._collect_pdf()

        if self.source_file_copied and not DEBUG:
            self._remove_old_file()
        return True

