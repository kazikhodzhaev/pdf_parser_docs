import cv2
import pytesseract
import time
import shutil
import subprocess
import glob
import logs
import os
import requests

from pymongo import MongoClient

from datetime import datetime
from elasticsearch import Elasticsearch, exceptions as es_exceptions
from pdf2image import convert_from_path, pdfinfo_from_path, exceptions
from read_pdf_conf_prod import *


logger = logs.get_logger(__name__, os.getpid())
client = MongoClient()
db = client.pdfParserDB


class LOG_TYPES:
    PARSE_PDF = 'parse_pdf'
    SCRIPT_WORKING_TIME = 'script_working_time'


def get_docs_path_to_parse(path_to_get_files, except_path=EXCEPT_PATH_TO_SCAN, check_date=True):
    """
    Получение списка файлов для парсинга
    :return:
    """
    files_list = []
    except_path_to_scan = []

    for path in os.listdir(path_to_get_files):
        except_path_to_scan.append(path) if path not in except_path else logger.info(f'EXCEPT PATH TO SCAN: {path}')

    for path in except_path_to_scan:
        files = glob.glob(f'{path_to_get_files}/{path}/**/*.pdf', recursive=True)
        # Добавляем найденые файлы в список для скана и парсинга
        for file in files:
            if not DEBUG and check_date:
                if not datetime.utcfromtimestamp(int(os.stat(file).st_mtime)) >= SCAN_FROM_DATE:
                    continue
            files_list.append(file)

    return files_list


def add_log(**kwargs):
    """
    Добавление лога в созданный файл
    :param file_path:
    :param kwargs:
    :return:
    """

    json_data = {
        "datetime": str(datetime.now().strftime('%Y-%m-%dT%H:%M:%S')),
        "file_path": kwargs.get('file_path', ""),
        "document_num": kwargs.get('document_num', ""),
        "document_type": kwargs.get('document_type', ""),
        "headers": kwargs.get('headers', ""),
        "error": kwargs.get('error', ""),
        "success": kwargs.get('success', ""),
        "msg": kwargs.get('msg', ""),
        "log_type": kwargs.get('log_type', ""),
        "method": kwargs.get('method', ""),
    }

    logger.info(json_data)
    if DEBUG:
        return True

    es = Elasticsearch([{'host': ELASTIC_SEARCH_HOST, 'port': ELASTIC_SEARCH_PORT,
                         'http_auth': f'{ELASTIC_SEARCH_USER}:{ELASTIC_SEARCH_PASS}'}])
    if es.ping():
        date_now = datetime.now()
        index = f'scandoc_{date_now.year}_{date_now.month}_{date_now.day}'
        id = hash(frozenset(json_data.items()))
        try:
            result = es.create(index, id, body=json_data)
            return result
        except es_exceptions.ConflictError:
            result = es.create(index, id+1, body=json_data)
            return result
        except es_exceptions.ConnectionTimeout:
            return False
    else:
        logger.error('Awww it could not connect!')
        return False


def make_dirs(guid):
    """
    Создаем папки при парсиге для каждого pdf файла которого парсим
    :param guid:
    :return: dirs
    """
    folder = datetime.now().date().__str__().replace('-', '')
    dirs = {
        "main": f"{PATH_TO_SAVE_PARSED_DOCS}/{folder}",
        "parsed_files": f"{PATH_TO_SAVE_PARSED_DOCS}/{folder}/{guid}",
        "In": f"{PATH_TO_SAVE_PARSED_DOCS}/{folder}/{guid}/In",
        "Out": f"{PATH_TO_SAVE_PARSED_DOCS}/{folder}/{guid}/Out",
        "Defected": f"{PATH_TO_SAVE_PARSED_DOCS}/{folder}/{guid}/Defected",
    }

    for v in dirs.values():
        os.makedirs(v) if not os.path.exists(v) else None

    os.chmod(dirs['Out'], 0o777)

    return dirs


def convert_pdf_to_img(pdf_file, path_save_img):
    """
    Конвертируем PDF файлы в изоброжение
    и сохроняем для дальнейшего парсинга
    :param pdf_file:
    :param path_save_img:
    :return:
    """
    start_time = time.time()
    image_counter = 0
    img_file_names = []

    try:
        info = pdfinfo_from_path(pdf_file, userpw=None, poppler_path=None)
    except exceptions.PDFPageCountError as e:
        add_log(file_path=pdf_file, msg=f"exceptions.PDFPageCountError: {e}", log_type=LOG_TYPES.PARSE_PDF,
                method=convert_pdf_to_img.__name__)
        data = {'file_path': pdf_file, 'msg': f"exceptions.PDFPageCountError: {e}", 'log_type': LOG_TYPES.PARSE_PDF,
                'method': convert_pdf_to_img.__name__}
        logger.error(data)

        logger.info('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        logger.info(f'REMOVE:{pdf_file}')
        logger.info('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        return image_counter, img_file_names

    max_pages = info["Pages"]

    for page in range(1, max_pages + 1, 5):
        pages = convert_from_path(pdf_file, dpi=400, first_page=page, last_page=min(page + 5 - 1, max_pages))

        for page in pages:
            filename = f"{path_save_img}/page_{image_counter}.jpeg"
            img_file_names.append(filename)
            page.save(filename, 'jpeg')
            image_counter = image_counter + 1

    add_log(file_path=pdf_file, method=convert_pdf_to_img.__name__, log_type=LOG_TYPES.SCRIPT_WORKING_TIME,
            msg=str(time.strftime('%H:%M:%S', time.gmtime(time.time() - start_time))))

    return image_counter, img_file_names


def get_text_from_image(file, number_recognize=False):
    """
    Распознование текса из изоброжения
    :param file:
    :param number_recognize:
    :return:
    """
    image = cv2.imread(file, cv2.IMREAD_GRAYSCALE)
    image = image[0:0+250, 0:3500]
    image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1] if number_recognize else cv2.bilateralFilter(image, 8, 60, 40)
    text = str(pytesseract.image_to_string(image, lang="rus"))
    text = text.replace('-\n', '').replace('\n', ' ')
    return text


def change_pdf_dpi(file, dpi='ebook'):
    """
    Меняем DPI файла PDF для того что бы уменьшить размер файла.
    :param file:
    :param dpi:
    :return:
    """
    if not os.path.exists(file):
        return False

    output_file = str(file).replace('.pdf', '_formatted.pdf')
    cmd = ['gs', '-sDEVICE=pdfwrite', '-dCompatibilityLevel=1.4', f'-dPDFSETTINGS=/{dpi}', '-dNOPAUSE', '-dQUIET',
           '-dBATCH', f'-sOutputFile={output_file}', f'{file}']
    subprocess.run(cmd)
    # Проверяем, файл валидный или нет
    try:
        pdfinfo_from_path(output_file, userpw=None, poppler_path=None)
    except (exceptions.PDFPageCountError, exceptions.PDFInfoNotInstalledError) as e:
        os.remove(output_file)
        return False
    # Если проверка валиднасти прошел успешно, продолжаем выполнение
    os.remove(file)
    os.rename(output_file, output_file.replace('_formatted.pdf', '.pdf'))
    return True


def create_or_update_one_c_req(json_data, doc_num, one_c_response=None):
    """
    Если при передачи результата в 1С произошла ошибка вызываем этот метод.
    Метод вносить данные в NoSQL базу для того что бы другая задача могла до отправить пока успешный ответ не получим
    :param json_data:
    :param doc_num:
    :param one_c_response:
    :return:
    """
    one_c_reqs = db.one_c_req_data.find({'document_number': doc_num})
    if one_c_reqs.count() > 0:
        for req in one_c_reqs:
            saved_data = db.one_c_req_data.update_one(
                {'_id': req['_id']},
                {'$set': {'response_code': one_c_response.status_code if one_c_response else 0,
                          'response_text': one_c_response.text if one_c_response else 'No'}})
            logger.info(f'SAVED ONE C RESPONSE: {saved_data}')
        return True
    else:
        data = {
            'body': json_data,
            'created_data': f'{datetime.now()}',
            'response_code': one_c_response.status_code if one_c_response else 0,
            'response_text': one_c_response.text if one_c_response else 'No',
            'document_number': doc_num
        }
        db.one_c_req_data.insert_one(data)
        return True


def send_req_in_to_one_c(data, doc_num=None):
    if not ONE_С_REQUEST_URL:
        if doc_num:
            create_or_update_one_c_req(json_data=data, doc_num=doc_num)
        return True
    response = requests.post(ONE_С_REQUEST_URL, json=data)
    if response.status_code == 200:
        logger.info(f'DATA RESEND IS SUCCESS: {data}')
    else:
        logger.error(f"1C RESPONSE: {response.status_code} | {response.text}")

    if doc_num:
        create_or_update_one_c_req(json_data=data, doc_num=doc_num, one_c_response=response)
    return response.status_code


def save_in_to_db_files_path(path, document_number):
    """
    Сохраняем пути для того что бы ковертор мог
    легко получить файл для изменение DPI
    :param path:
    :param document_number:
    :return:
    """
    data = {
        'path': path,
        'created_data': f'{datetime.now()}',
        'is_convert': 0,
        'document_number': document_number
    }
    db.reformated_pdf_paths.insert_one(data)
    return True

