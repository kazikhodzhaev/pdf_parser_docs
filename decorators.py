import time

from utils import LOG_TYPES, add_log


def timing(func):
    """
    Получение время работы скрипта
    :return:
    """
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        script_work_seconds = time.time() - start_time
        msg = str(time.strftime('%H:%M:%S', time.gmtime(script_work_seconds)))
        add_log(log_type=LOG_TYPES.SCRIPT_WORKING_TIME, msg=msg, method=func.__name__)
        return result
    return wrapper




