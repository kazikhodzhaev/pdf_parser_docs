import logging

_log_format = f"%(asctime)s - [%(levelname)s] - %(name)s - (%(filename)s).%(funcName)s(%(lineno)d) - %(message)s"


def get_file_handler_info():
    file_handler_warning = logging.FileHandler('pdf_parser_info.log')
    file_handler_warning.setLevel(logging.INFO)
    file_handler_warning.setFormatter(logging.Formatter(_log_format))
    return file_handler_warning


def get_file_handler_error():
    file_handler_warning = logging.FileHandler('pdf_parser_error.log')
    file_handler_warning.setLevel(logging.ERROR)
    file_handler_warning.setFormatter(logging.Formatter(_log_format))
    return file_handler_warning


def get_logger(name, pid):
    logger = logging.getLogger(f'{name} | {pid}')
    logger.setLevel(logging.INFO)
    logger.addHandler(get_file_handler_info())
    logger.addHandler(get_file_handler_error())
    return logger

