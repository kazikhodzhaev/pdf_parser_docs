#!/usr/bin/env python3.6
import multiprocessing as mp
import logs

from pymongo import MongoClient
from read_pdf_conf_prod import *
from utils import convert_pdf_to_img, get_docs_path_to_parse, send_req_in_to_one_c, change_pdf_dpi
from read_pdf import ReadAndParseScanDocs
from decorators import timing

logger = logs.get_logger(__name__, os.getpid())
client = MongoClient()
db = client.pdfParserDB


@timing
def resend():
    logger.info('============== Start resend data ==============')
    one_c_req_data = db.one_c_req_data
    error_reqs = one_c_req_data.find({'response_code': {'$ne': 200}})
    for data in error_reqs:
        send_req_in_to_one_c(data=data["body"], doc_num=data["document_number"])

    one_c_reqs = one_c_req_data.find({})
    for data in one_c_reqs:
        logger.info(f'RESEND DATA INTO ONE_С: {data}')
    logger.info('============== End resend data ==============')
    return True


@timing
def convert():
    logger.info('============== Start convert data ==============')
    files = db.reformated_pdf_paths.find({'is_convert': 0})
    for file in files:
        result = change_pdf_dpi(file=file['path'])
        if result:
            db.reformated_pdf_paths.update_one({'path': file['path']}, {'$set': {'is_convert': 1}})
    logger.info('============== End resend data ==============')
    return True


@timing
def main(files_lists: list):
    """
    Запуск парсинга PDF файлов и сборки
    :return:
    """
    path_save_img = f'{PARSED_IMG_SAVE_PATH}/{os.getpid()}'
    is_exists = lambda data: True if data else False
    os.makedirs(path_save_img) if not os.path.exists(path_save_img) else None

    for pdf_file in files_lists:
        logger.info(pdf_file)
        # Конвертирование PDF файла в png файлы
        image_counter, img_file_names = convert_pdf_to_img(pdf_file=pdf_file, path_save_img=path_save_img)

        if not img_file_names:
            continue

        parse = ReadAndParseScanDocs(file=pdf_file, path_save_img=path_save_img)
        parse.run()
        # Удаляем все png файлы после того как распарсили
        for img_file_name in img_file_names:
            os.remove(img_file_name) if is_exists(img_file_names) else None

        files_list.remove(pdf_file)

    os.rmdir(path_save_img)
    return None


if __name__ == '__main__':
    files_list = get_docs_path_to_parse(path_to_get_files=PATH_TO_GET_DOCS)
    logger.info(f'PDF FILES FOR PARSE:\n {files_list}')
    list_half = round(len(files_list) / 3)
    files_list_part = [
        files_list[:list_half],
        files_list[list_half:list_half + list_half],
        files_list[list_half + list_half:]
    ]
    # Разделяем на 3 потока парсинг
    for part in files_list_part:
        proc = mp.Process(target=main, args=(part, ))
        proc.start()
        proc.join()

    convert()
    resend()


